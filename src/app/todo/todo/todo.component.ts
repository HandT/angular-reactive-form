import { Component, forwardRef, Input, OnChanges, OnInit } from '@angular/core';
import { BaseControlValueAccessor } from '../../BaseControlValueAccessor';
import { Todo } from '../../app.component';
import { FormGroup, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, Validators } from '@angular/forms';

@Component({
	selector: 'todo',
	template: `
		<div [formGroup]="todoGroup">
			<check formGroupName="check" [parentForm]="todoGroup"></check>
			<input formControlName="task" />
			<span>{{todoGroup.value | json}}</span>
			<button [disabled]="!todoGroup.valid">TODO</button>
		</div>
	`,
	styles: [``],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => TodoComponent),
			multi: true
		}
	]
})
export class TodoComponent extends BaseControlValueAccessor<Todo>
	implements ControlValueAccessor, OnInit, OnChanges {
	setDisabledState(isDisabled: boolean): void {
	}
	todoGroup: FormGroup;

	@Input() parentForm: FormGroup;

	constructor(private formBuilder: FormBuilder) {
		super();

		//this.todoGroup = formBuilder.group({
		//	task: ['', Validators.required],
		//	//selected: ['ahihi', Validators.required]
		//});
		//this.parentForm.addControl('todo', this.todoGroup);

		//this.todoGroup.valueChanges.subscribe(val => {
		//  this.value = val;
		//  this.onChange(val);
		//});

		this.todoGroup = this.formBuilder.group({
			task: ['', Validators.required],
			check: new FormGroup({}),
			//selected: ['ahihi', Validators.required]
		});
	}
	//this.todoGroup.patchValue(todo);

	writeValue(todo: Todo) {
		this.value = todo;
	}

	ngOnInit(): void {
		this.parentForm.removeControl('todo');
		this.parentForm.addControl('todo', this.todoGroup);
	}

	ngOnChanges(changes: any): void {
		//this.todoGroup = this.formBuilder.group({
		//	task: ['', Validators.required],
		//	check: new FormGroup({}),
		//	//selected: ['ahihi', Validators.required]
		//});
		//this.parentForm.addControl('todo', this.todoGroup);
	}
}
