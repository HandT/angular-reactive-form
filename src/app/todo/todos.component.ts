import { Component, forwardRef, Input } from '@angular/core';
import {Todo} from '../app.component';
import {BaseControlValueAccessor} from '../BaseControlValueAccessor';
import { FormGroup, FormBuilder, FormArray, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'todos',
  template: `
    <form [formGroup]="todosGroup">
      <div formArrayName="todos">
        <todo *ngFor="let todo of todosArr.controls; let i=index"[formControlName]="i"></todo>
      </div>
      <input type="button" (click)="newTodo()" value="New todo"/>
		<button [disabled]="!todosGroup.valid">TODOS</button>
    </form>
  `,
  styles: [`
    form {
      margin: 24px;
      padding: 8px;
      border: solid 1px black;
      display: inline-block;
    }
    div { display: inline-block; }

    input {
      display: inline-block;
    }
  `],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TodosComponent),
      multi: true
    }
  ]
})
export class TodosComponent extends BaseControlValueAccessor<Todo[]> {
  todosGroup: FormGroup;
  @Input() name: string;
  get todosArr(): FormArray { return this.todosGroup.get('todos') as FormArray; }

  constructor(private formBuilder: FormBuilder) {
    super();

    this.todosGroup = formBuilder.group({
      todos: formBuilder.array([{task: '', selected: 'ahihi'}])
    });

    this.todosGroup.valueChanges.subscribe(val => {
      this.onChange(val);
      this.value = val.todos;
    })
  }

  writeValue(todos: Todo[]) {
    super.writeValue(todos);

    // this sucks, maybe there's a better way, just trying to 
    // set the value of the array to the new value
    while(this.todosArr.length > 0) { this.todosArr.removeAt(0); }
    todos.forEach(todo => this.todosArr.push(this.formBuilder.control(todo)));
  }

  newTodo() {
    this.writeValue(this.value.concat({task: 'new todo', selected: 'ahihi'}));
  }
}
