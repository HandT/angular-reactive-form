import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { BaseControlValueAccessor } from '../../BaseControlValueAccessor';
import { Todo } from '../../app.component';

@Component({
	selector: 'check',
	template: `
		<div [formGroup]="checkGroup">check
			<input formControlName="selected" />
			<button [disabled]="!checkGroup.valid">CHECK</button>
		</div>`,
	styles: [`
		div {
			width: 50px;
			height: 24px;
			border: 1px solid black;
			margin-right: 24px;
			display: inline-block;
		}

		.check-selected {
			background-color: black;
		}
	`],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => CheckComponent),
			multi: true
		}
	]
})
export class CheckComponent extends BaseControlValueAccessor<string>
	implements OnInit {
	setDisabledState(isDisabled: boolean): void {
	}

	checkGroup: FormGroup;

	@Input() parentForm: FormGroup;
	constructor(private formBuilder: FormBuilder) {
		super();
		//this.checkGroup = formBuilder.group({
		//	selected: ['', Validators.required]
		//});
		//this.parentForm.addControl('check', this.checkGroup);
		this.checkGroup = this.formBuilder.group({
			selected: ['', Validators.required]
		});
	}

	writeValue(value: string) {

	}

	ngOnInit(): void {
		this.parentForm.removeControl('check');
		this.parentForm.addControl('check', this.checkGroup);
	}
}
