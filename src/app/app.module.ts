import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {TodosComponent} from './todo/todos.component';
import {CheckComponent} from './todo/check/check.component';
import {TodoComponent} from './todo/todo/todo.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule, ReactiveFormsModule ],
  declarations: [ AppComponent, TodoComponent, CheckComponent, TodosComponent, TodoComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
