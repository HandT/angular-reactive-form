import { Component, OnChanges, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

export interface Todo {
	task: string;
	selected: string;
}

const defaultTodos: Todo[] = [{ task: 'mow lawn', selected: 'ahihi' }, { task: 'buy stuff', selected: 'ahihi' }];

const defaultTodos2: Todo[] = [{ task: 'mow lawn', selected: 'ahihi' }, { task: 'buy stuff', selected: 'ahihi' }];

@Component({
	selector: 'my-app',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnChanges {
	mainGroup: FormGroup;
	constructor(private formBuilder: FormBuilder) {
		//this.mainGroup = formBuilder.group({
		//	//todoList: [defaultTodos],
		//	//anotherTodoList: [defaultTodos2]
		//})
		this.mainGroup = this.formBuilder.group({
			'todo': new FormGroup({}),
		});
	}

	ngOnInit(): void {
		//this.mainGroup = this.formBuilder.group({
		//	todo: new FormGroup({}),
		//	//todoList: [defaultTodos],
		//	//anotherTodoList: [defaultTodos2]
		//})
		//this.mainGroup.addControl('todo', new FormGroup({}));

		this.mainGroup.valueChanges.subscribe(val => {
		  console.log(val);
		});
	}

	ngOnChanges(changes: any): void {

	}

	onSubmit() {
		console.log(this.mainGroup);
	}

}
